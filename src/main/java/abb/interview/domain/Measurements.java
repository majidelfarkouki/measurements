package abb.interview.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class Measurements extends HashMap<Key, Measurement>{

    public Measurement find(Key key) {
        return get(key);
    }
    
    public void printMeasurements(Measurements measurements) {
    	List<Measurement> devicesGroupA = new ArrayList<Measurement>();    	    
	    List<Measurement> devicesGroupB = new ArrayList<Measurement>();    	    
	    
	    List<Power> powerDevicesGroupAIn = new ArrayList<Power>();    	    
	    List<Power> powerDevicesGroupAOut = new ArrayList<Power>();    	    

	    List<Power> powerDevicesGroupBIn = new ArrayList<Power>(); 
	    List<Power> powerDevicesGroupBOut = new ArrayList<Power>(); 

	    double totalPowerAvgGroupAIn = 0;
	    double totalPowerAvgGroupAOut = 0;
	    double totalPowerAvgGroupBIn = 0;
	    double totalPowerAvgGroupBOut = 0;	 
	    
	    for (Measurement measurement : measurements.values()) {
	    	if(measurement.getDeviceGroup().equals("group_a")) {
	    		devicesGroupA.add(measurement);
	    	}
	    	
	    	if(measurement.getDeviceGroup().equals("group_b")) {
	    		devicesGroupB.add(measurement);
	    	}
	    }
		
	    for(int i = 0; i < devicesGroupA.size(); i++) {
	    	if(devicesGroupA.get(i).getDirection().equals(Direction.IN)) {
	    	    List<Power> powers = devicesGroupA.get(i).getPower();    	    
	    	    powers.forEach(power -> {
	    	    	powerDevicesGroupAIn.add(power);
	    	    });    	    	
	    	}
	    	
	    	if(devicesGroupA.get(i).getDirection().equals(Direction.OUT)) {
	    	    List<Power> powers = devicesGroupA.get(i).getPower();    	    
	    	    powers.forEach(power -> {
	    	    	powerDevicesGroupAOut.add(power);
	    	    });    	    	
	    	}
	    }

	    for(int i = 0; i < devicesGroupB.size(); i++) {
	    	if(devicesGroupB.get(i).getDirection().equals(Direction.IN)) {
	    	    List<Power> powers = devicesGroupB.get(i).getPower();    	    
	    	    powers.forEach(power -> {
	    	    	powerDevicesGroupBIn.add(power);
	    	    });    	    	
	    	}
	    	if(devicesGroupB.get(i).getDirection().equals(Direction.OUT)) {
	    	    List<Power> powers = devicesGroupB.get(i).getPower();    	    
	    	    powers.forEach(power -> {
	    	    	powerDevicesGroupBOut.add(power);
	    	    });    	    	
	    	}
	    }
	    
	    for (Power power : powerDevicesGroupAIn) {
	    	totalPowerAvgGroupAIn += power.getAvg();
	    }
	    
	    for (Power power : powerDevicesGroupAOut) {
	    	totalPowerAvgGroupAOut += power.getAvg();
	    }
	    
	    for (Power power : powerDevicesGroupBIn) {
	    	totalPowerAvgGroupBIn += power.getAvg();
	    }
	    
	    for (Power power : powerDevicesGroupBOut) {
	    	totalPowerAvgGroupBOut += power.getAvg();
	    }
	    
		 BigDecimal bd1 = new BigDecimal(totalPowerAvgGroupAIn).setScale(4,RoundingMode.HALF_DOWN);
		 totalPowerAvgGroupAIn = bd1.doubleValue();

		 BigDecimal bd2 = new BigDecimal(totalPowerAvgGroupAOut).setScale(4,RoundingMode.HALF_DOWN);
		 totalPowerAvgGroupAOut = bd2.doubleValue();
		 
		 BigDecimal bd3 = new BigDecimal(totalPowerAvgGroupBIn).setScale(4,RoundingMode.HALF_DOWN);
		 totalPowerAvgGroupBIn = bd3.doubleValue();
		 
		 BigDecimal bd4 = new BigDecimal(totalPowerAvgGroupBOut).setScale(4,RoundingMode.HALF_DOWN);
		 totalPowerAvgGroupBOut = bd4.doubleValue();
		 
	    String str1 = "Group A -> Direction IN  -> Average total power : " + totalPowerAvgGroupAIn;
	    String str2 = "Group A -> Direction OUT -> Average total power : " + totalPowerAvgGroupAOut;
	    String str3 = "Group B -> Direction IN  -> Average total power : " + totalPowerAvgGroupBIn;
	    String str4 = "Group B -> Direction OUT -> Average total power : " + totalPowerAvgGroupBOut;

	    System.out.print(str1 + "\n" +str2 + "\n" +str3 + "\n" +str4);
    }
    
    public List<Measurement> outputSortedMeasurements(Measurements measurements) {
    	List<Measurement> sortedMeasurementsList = new ArrayList<Measurement>();   			  
			  
   	    for (Measurement measurement : measurements.values()) {
 	    		 List<Power> power = measurement.getPower();	    		 
 	    		 Collections.sort(power,new Comparator<Power>() {
 	    			 public int compare(Power p1, Power p2) {
 	    			     return new Double(p1.getMax()).compareTo(new Double(p2.getMax()));
 	    			 }
 	    	});
 	    		 
 	             Power maxPower = power.get(power.size() - 1);
 	             BigDecimal bd1 = new BigDecimal(maxPower.getMin()).setScale(4,RoundingMode.HALF_DOWN);
 	             			maxPower.setMin(bd1.doubleValue());
 	             BigDecimal bd2 = new BigDecimal(maxPower.getMax()).setScale(4,RoundingMode.HALF_DOWN);
 	             			maxPower.setMax(bd2.doubleValue());
 	             BigDecimal bd3 = new BigDecimal(maxPower.getAvg()).setScale(4,RoundingMode.HALF_DOWN);
 	             			maxPower.setAvg(bd3.doubleValue());
 	             			
 	             List<Power> p = new ArrayList<Power>();		
 	             			 p.add(maxPower);
 	             Measurement m = new Measurement();
 	             	m.setResourceId(measurement.getResourceId());
 	             	m.setDeviceName(measurement.getDeviceName());
 	             	m.setDeviceGroup(measurement.getDeviceGroup());
 	             	m.setDirection(measurement.getDirection());
 	             	m.setPower(p);
 	             	
 	             sortedMeasurementsList.add(m);	             	
   	    }
   	    
   	  Collections.sort(sortedMeasurementsList,new Comparator<Measurement>() {
 			 public int compare(Measurement m1, Measurement m2) {
 				 int c;
 				    c = m1.getDeviceGroup().compareTo(m2.getDeviceGroup());
 				    if (c == 0)
 				       c = m1.getDirection().compareTo(m2.getDirection());
 				    if (c == 0)
 				        c = Double.compare(m1.getPower().get(0).getMax(),m2.getPower().get(0).getMax());
 				    return c;
 			 }
 		 });
 
   		return sortedMeasurementsList;
    }
}
