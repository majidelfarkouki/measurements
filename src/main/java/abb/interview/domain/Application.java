package abb.interview.domain;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Application {

    private final Measurements measurements = new Measurements();
    
    public Measurements readJSONFile() throws JsonParseException, JsonMappingException, IOException {
	    ObjectMapper mapper = new ObjectMapper();
	    //Change file path
        Path path = Paths.get("C:\\Users\\aelfarko\\java-measurements\\measurements\\src\\main\\resources\\measurements.json");
        List<Measurement> measurementsList = Arrays.asList(mapper.readValue(path.toFile(), Measurement[].class));
	    for (Measurement measurement : measurementsList) {
	    	Key k = new Key(measurement.getResourceId(), measurement.getDeviceName(), measurement.getDeviceGroup());
	    	measurements.put(k,measurement);
	    }
	    return measurements;
	}
    
    public static void main(String[] args) {

    	Application a = new Application();
	    Measurements m;
	    try {
			m = a.readJSONFile();
			m.printMeasurements(m);
			m.outputSortedMeasurements(m);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    }
}
