package abb.interview.domain.test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import abb.interview.domain.Direction;
import abb.interview.domain.Key;
import abb.interview.domain.Measurement;
import abb.interview.domain.Measurements;
import abb.interview.domain.Power;
import junit.framework.Assert;

public class MeasurementsTest {

	private Measurements measurements;
    private List<Measurement> sortedMeasurementsList;
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    
	@Before
	public void setUp() throws Exception {
		measurements = new Measurements();
		sortedMeasurementsList = new ArrayList<Measurement>();
	    System.setOut(new PrintStream(outputStreamCaptor));
	    
		List<Power> p1 = new ArrayList<Power>();
					p1.add(new Power(29.163184452205893,65.44317306454077,47.303178758373335,1626300000));
					p1.add(new Power(13.620455434219808,99.74979414798125,56.68512479110053,1626300900));
					p1.add(new Power(23.37824512445404,90.90748071088007,57.14286291766705,1626302700));			
		List<Power> p2 = new ArrayList<Power>();
					p2.add(new Power(43.369282856064075,63.832139591421075,53.60071122374258,1626300000));
					p2.add(new Power(43.70940488552223,60.62410635020656,52.166755617864396,1626300900));
		Measurement m1 = new Measurement();
					m1.setResourceId("49e258fe-9d40-43b4-9a29-31f82ad5ec15");
					m1.setDeviceName("device_1");
					m1.setDeviceGroup("group_a");
					m1.setDirection(Direction.OUT);
					m1.setPower(p1);
		Measurement m2 = new Measurement();
					m2.setResourceId("4d5b9f5e-2b18-41bd-96f0-96674ed9846a");
					m2.setDeviceName("device_2");
					m2.setDeviceGroup("group_a");
					m2.setDirection(Direction.IN);
					m2.setPower(p2);
		List<Power> p3 = new ArrayList<Power>();
					p3.add(new Power(31.64977673439958,54.480270481616174,43.06502360800788,1626300000));
					p3.add(new Power(43.90603252096313,78.06630554765299,60.98616903430806,1626300900));
					p3.add(new Power(28.755453795141456,91.3119701057546,60.03371195044802,1626302700));			
		List<Power> p4 = new ArrayList<Power>();
					p4.add(new Power(13.344991978543373,58.41175097246223,35.8783714755028,1626300000));
					p4.add(new Power(15.745400374920928,54.881252244183315,35.313326309552124,1626300900));
		Measurement m3 = new Measurement();
					m3.setResourceId("75ca6be9-1040-4797-b62d-74a41cae7655");
					m3.setDeviceName("device_3");
					m3.setDeviceGroup("group_b");
					m3.setDirection(Direction.IN);
					m3.setPower(p3);
		Measurement m4 = new Measurement();
					m4.setResourceId("f81beded-44e4-46bb-aaf7-7d213fa9b045");
					m4.setDeviceName("device_4");
					m4.setDeviceGroup("group_b");
					m4.setDirection(Direction.OUT);
					m4.setPower(p4);
					
	   measurements.put(new Key(m1.getResourceId(), m1.getDeviceName(), m1.getDeviceGroup()), m1);
	   measurements.put(new Key(m2.getResourceId(), m2.getDeviceName(), m2.getDeviceGroup()), m2);
	   measurements.put(new Key(m3.getResourceId(), m3.getDeviceName(), m3.getDeviceGroup()), m3);
	   measurements.put(new Key(m4.getResourceId(), m4.getDeviceName(), m4.getDeviceGroup()), m4);
	
	   List<Power> sp1 = new ArrayList<Power>();
		sp1.add(new Power(13.6205,99.7498,56.6851,1626300900));
		
	   List<Power> sp2 = new ArrayList<Power>();
		sp2.add(new Power(43.3693,63.8321,53.6007,1626300000));
		
	   List<Power> sp3 = new ArrayList<Power>();
		sp3.add(new Power(28.7555,91.3120,60.0337,1626302700));
		
	   List<Power> sp4 = new ArrayList<Power>();
		sp4.add(new Power(13.3450,58.4118,35.8784,1626300000));

	   Measurement sm1 = new Measurement();
		sm1.setResourceId("49e258fe-9d40-43b4-9a29-31f82ad5ec15");
		sm1.setDeviceName("device_1");
		sm1.setDeviceGroup("group_a");
		sm1.setDirection(Direction.OUT);
		sm1.setPower(sp1);
	  Measurement sm2 = new Measurement();
		sm2.setResourceId("4d5b9f5e-2b18-41bd-96f0-96674ed9846a");
		sm2.setDeviceName("device_2");
		sm2.setDeviceGroup("group_a");
		sm2.setDirection(Direction.IN);
		sm2.setPower(sp2);
		
	  Measurement sm3 = new Measurement();
		sm3.setResourceId("75ca6be9-1040-4797-b62d-74a41cae7655");
		sm3.setDeviceName("device_3");
		sm3.setDeviceGroup("group_b");
		sm3.setDirection(Direction.IN);
		sm3.setPower(sp3);
      Measurement sm4 = new Measurement();
		sm4.setResourceId("f81beded-44e4-46bb-aaf7-7d213fa9b045");
		sm4.setDeviceName("device_4");
		sm4.setDeviceGroup("group_b");
		sm4.setDirection(Direction.OUT);
		sm4.setPower(sp4);
	   
		sortedMeasurementsList.add(sm2);
		sortedMeasurementsList.add(sm1);	
		sortedMeasurementsList.add(sm3);	
		sortedMeasurementsList.add(sm4);	
	}

	@After
	public void tearDown() throws Exception {
		measurements.clear();
		sortedMeasurementsList.clear();
	    System.setOut(standardOut);
	}

	@Test
	public void testPrintMeasurements() {
		measurements.printMeasurements(measurements);
        
	    Assert.assertEquals("Group A -> Direction IN  -> Average total power : 105.7675\n"
	    		+ "Group A -> Direction OUT -> Average total power : 161.1312\n"
	    		+ "Group B -> Direction IN  -> Average total power : 164.0849\n"
	    		+ "Group B -> Direction OUT -> Average total power : 71.1917", outputStreamCaptor.toString());
	}

	@Test
	public void testOutputSortedMeasurements() {
		List<Measurement> list1 =sortedMeasurementsList;
		List<Measurement> list2  = measurements.outputSortedMeasurements(measurements);
		boolean b = false;
		
		if (list1.size() == list2.size()) {
				for(int i=0; i<list1.size(); i++) {
					if(list1.get(i).getResourceId().equals(list2.get(i).getResourceId()) 
					   && list1.get(i).getDeviceName().equals(list2.get(i).getDeviceName())
					   && list1.get(i).getDeviceGroup().equals(list2.get(i).getDeviceGroup())
					   && list1.get(i).getDirection().equals(list2.get(i).getDirection())
					   && list1.get(i).getPower().get(0).getMax() == list2.get(i).getPower().get(0).getMax()){
						b = true;
					}
					else {
						b = false;
						break;
					}
				}
            }
		 else {
			b = false;
		   }
 
	     Assert.assertTrue(b == true);
	}

}
